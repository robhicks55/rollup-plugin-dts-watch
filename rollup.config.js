import json from 'rollup-plugin-json';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonJs from 'rollup-plugin-commonjs';
import stylus from 'rollup-plugin-stylus-to-css';
import glob from 'glob';
import { ts, dts } from 'rollup-plugin-dts';

const mjsFiles = glob.sync('src/**/*.mjs');
const tsFiles = glob.sync('src/**/*.ts');
let configs = [];

const plugins = [ nodeResolve(), commonJs(), stylus(), json() ];

const tsConfigs = tsFiles.map(input => {
  const fileName = input.match(/.+\/(.+)/)[1].replace('.ts', '.js');
  return {
    input,
    plugins: [ ...plugins, ts() ],
    output: { file: `./dist/${fileName}`, format: 'es' }
  };
});

const dtsConfigs = tsFiles.map(input => {
  const fileName = input.match(/.+\/(.+)/)[1].replace('.ts', '.d.ts');
  return {
    input,
    plugins: [ ...plugins, dts() ],
    output: { file: `./dist/${fileName}`, format: 'es' }
  };
});

if (process.argv.some(key => (key === '-wc' || key === '-cw'))) {
  const liveServer = require('rollup-plugin-live-server');

  const server = liveServer({
    file: 'index.html',
    logLevel: 2,
    mount: [[ '/dist', './dist' ], [ '/src', './src' ], [ '/node_modules', './node_modules' ], [ '/test', './test' ]],
    open: false,
    port: 8080,
    root: 'demo',
    verbose: false,
    wait: 500
  });

  tsConfigs[0].plugins = [...tsConfigs[0].plugins, server]

  configs = [ ...tsConfigs, ...dtsConfigs ];
} else {
  configs = [ ...tsConfigs, ...dtsConfigs ];
}

export default configs;
