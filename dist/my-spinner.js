class MySpinner extends HTMLElement {
    constructor() {
        super();
        this.viewElement = this.attachShadow ? this.attachShadow({ mode: 'open' }) : this;
        return this;
    }
    attributeChangedCallback(name, oVal, nVal) {
        this[`_${name}`] = nVal;
        this.render();
    }
    connectedCallback() {
        this.render();
        this.spinner.classList.add('hidden');
    }
    destroy() {
        this.remove();
    }
    render() {
        this.viewElement.innerHTML = `
        <style>
        svg {
          animation-name: spin;
          animation-duration: 400ms;
          animation-iteration-count: infinite;
          animation-timing-function: linear;
        }
        .hidden {
          display: none;
        }
        @keyframes spin {
          from { transform:rotate(0deg) }
          to { transform:rotate(360deg) }
        }
        </style>
        <svg width="${this.size}" height="${this.size}" viewBox="0 0 32 32"><g fill="${this.color}" fill-rule="nonzero"><path d="M15 2h2v7h-2zM15 23h2v7h-2zM5.393 6.808l1.414-1.415 4.95 4.95-1.414 1.414zM20.243 21.657l1.414-1.414 4.95 4.95-1.414 1.414zM25.193 5.393l1.414 1.415-4.95 4.95-1.414-1.415zM10.343 20.243l1.414 1.414-4.95 4.95-1.414-1.415zM30 15v2h-7v-2zM9 15v2H2v-2z"/></g></svg>
      `;
        this.spinner = this.viewElement.querySelector('svg');
        // bug in typescript: https://www.w3.org/TR/SVG2/styling.html#ElementSpecificStyling
        this.spinner.style = `height: ${this.size}; width: ${this.size};`;
    }
    spinning() {
        return this.spinner && this.spinner.classList && this.spinner.classList.contains ? !this.spinner.classList.contains('hidden') : false;
    }
    start() {
        this.spinner.classList.remove('hidden');
        return this;
    }
    stop() {
        this.spinner.classList.add('hidden');
        return this;
    }
    get size() {
        return this._size || '64';
    }
    set size(value) {
        this.setAttribute('size', value);
    }
    get color() {
        return this._color || '#FFFF00';
    }
    set color(value) {
        this.setAttribute('color', value);
    }
    static get observedAttributes() {
        return [
            'color',
            'size'
        ];
    }
}
customElements.get('my-spinner') || customElements.define('my-spinner', MySpinner);

export { MySpinner };
