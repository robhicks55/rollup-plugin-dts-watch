document.addEventListener('DOMContentLoaded', () => {
  const create = document.querySelector('button[name="create"]');
  const show = document.querySelector('button[name="show"]');
  const hide = document.querySelector('button[name="hide"]');
  const destroy = document.querySelector('button[name="destroy"]');
  const red = document.querySelector('button[name="red"]');
  const resize = document.querySelector('button[name="resize"]');
  const container = document.querySelector('#spinner-container');

  let spinner;

  function createSpinner() {
    if (spinner) return spinner;
    spinner = container.appendChild(document.createElement('my-spinner'));
    console.log(`spinner`, spinner)
    return spinner;
  }

  create.addEventListener('click', createSpinner);

  show.addEventListener('click', () => {
    if (!spinner) createSpinner();
    spinner.start();
  });

  hide.addEventListener('click', () => {
    if (spinner) spinner.stop();
  });

  destroy.addEventListener('click', () => {
    if (spinner) spinner.destroy();
    spinner = null;
  });

  red.addEventListener('click', () => {
    if (spinner) spinner.color = '#FF3333';
  });

  resize.addEventListener('click', () => {
    if (spinner) spinner.size = '36px';
  });
});
